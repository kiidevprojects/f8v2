-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: dbhotel
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bsi_admin`
--

DROP TABLE IF EXISTS `bsi_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pass` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT 'admin',
  `access_id` int(1) NOT NULL DEFAULT '0',
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_admin`
--

LOCK TABLES `bsi_admin` WRITE;
/*!40000 ALTER TABLE `bsi_admin` DISABLE KEYS */;
INSERT INTO `bsi_admin` VALUES (1,'21232f297a57a5a743894a0e4a801fc3','admin',1,'admin','admin','admin@example.com','Administrator','2019-04-03 07:29:06',1);
/*!40000 ALTER TABLE `bsi_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_adminmenu`
--

DROP TABLE IF EXISTS `bsi_adminmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_adminmenu` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `parent_id` int(4) DEFAULT '0',
  `status` enum('Y','N') CHARACTER SET latin1 DEFAULT 'Y',
  `ord` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_adminmenu`
--

LOCK TABLES `bsi_adminmenu` WRITE;
/*!40000 ALTER TABLE `bsi_adminmenu` DISABLE KEYS */;
INSERT INTO `bsi_adminmenu` VALUES (6,'SETTING','#',0,'Y',9),(31,'Global Setting','global_setting.php',6,'Y',1),(33,'HOTEL MANAGER','#',0,'Y',2),(34,'Room Manager','room_list.php',33,'Y',1),(35,'RoomType Manager','roomtype.php',33,'Y',2),(36,'PricePlan Manager','priceplan.php',63,'Y',4),(37,'BOOKING MANAGER','#',0,'Y',4),(39,'View Booking List','view_bookings.php',37,'Y',2),(43,'Payment Gateway','payment_gateway.php',6,'Y',4),(44,'Email Contents','email_content.php',6,'Y',5),(59,'Capacity Manager','admin_capacity.php',33,'Y',3),(61,'Advance Payment','advance_payment.php',63,'Y',6),(63,'PRICE MANAGER','#',0,'Y',3),(66,'Hotel Details','admin_hotel_details.php',33,'Y',0),(68,'Room Blocking','admin_block_room.php',37,'Y',6),(70,'Calendar View','calendar_view.php',37,'Y',5),(71,'Customer Lookup','customerlookup.php',37,'Y',4),(72,'Admin Menu Manager','adminmenu.list.php',6,'Y',6),(73,'LANGUAGE MANAGER','#',0,'Y',6),(74,'Manage Languages','manage_langauge.php',73,'Y',1),(75,'Special Offer','view_special_offer.php',63,'Y',5),(76,'CURRENCY MANAGER','#',0,'Y',7),(77,'Manage Currency','currency_list.php',76,'Y',1),(78,'Photo Gallery','gallery_list.php',33,'Y',7);
/*!40000 ALTER TABLE `bsi_adminmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_advance_payment`
--

DROP TABLE IF EXISTS `bsi_advance_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_advance_payment` (
  `month_num` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(255) NOT NULL,
  `deposit_percent` decimal(10,2) NOT NULL,
  PRIMARY KEY (`month_num`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_advance_payment`
--

LOCK TABLES `bsi_advance_payment` WRITE;
/*!40000 ALTER TABLE `bsi_advance_payment` DISABLE KEYS */;
INSERT INTO `bsi_advance_payment` VALUES (1,'January',0.00),(2,'February',0.00),(3,'March',0.00),(4,'April',0.00),(5,'May',0.00),(6,'June',0.00),(7,'July',0.00),(8,'August',0.00),(9,'September',0.00),(10,'October',0.00),(11,'November',0.00),(12,'December',0.00);
/*!40000 ALTER TABLE `bsi_advance_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_bookings`
--

DROP TABLE IF EXISTS `bsi_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_bookings` (
  `booking_id` int(10) unsigned NOT NULL,
  `booking_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `client_id` int(10) unsigned DEFAULT NULL,
  `child_count` int(2) NOT NULL DEFAULT '0',
  `extra_guest_count` int(2) NOT NULL DEFAULT '0',
  `discount_coupon` varchar(50) DEFAULT NULL,
  `total_cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `payment_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_type` varchar(255) NOT NULL,
  `payment_success` tinyint(1) NOT NULL DEFAULT '0',
  `payment_txnid` varchar(100) DEFAULT NULL,
  `paypal_email` varchar(500) DEFAULT NULL,
  `special_id` int(10) unsigned NOT NULL DEFAULT '0',
  `special_requests` text,
  `is_block` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `block_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `booking_time` (`discount_coupon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_bookings`
--

LOCK TABLES `bsi_bookings` WRITE;
/*!40000 ALTER TABLE `bsi_bookings` DISABLE KEYS */;
INSERT INTO `bsi_bookings` VALUES (1554368573,'2019-04-04 17:02:53','2019-04-04','2019-04-05',1,0,0,NULL,6600.00,6600.00,'poa',1,NULL,NULL,0,'',0,0,NULL),(1554368890,'2019-04-04 17:08:10','2019-04-05','2019-04-10',2,0,0,NULL,94050.00,94050.00,'poa',1,NULL,NULL,0,'efadf',0,0,NULL);
/*!40000 ALTER TABLE `bsi_bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_capacity`
--

DROP TABLE IF EXISTS `bsi_capacity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_capacity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_capacity`
--

LOCK TABLES `bsi_capacity` WRITE;
/*!40000 ALTER TABLE `bsi_capacity` DISABLE KEYS */;
INSERT INTO `bsi_capacity` VALUES (4,'Mulawin',2),(6,'Duhat',4),(7,'Manga 1',2),(8,'Manga 2',4),(9,'Ipil',2),(10,'Anapla',4),(11,'Giho',2),(12,'Dayap',2),(13,'Dalumpit',2),(14,'Bignay',2),(15,'Kamatsili',60);
/*!40000 ALTER TABLE `bsi_capacity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_cc_info`
--

DROP TABLE IF EXISTS `bsi_cc_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_cc_info` (
  `booking_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `card_type` varchar(50) NOT NULL,
  `card_number` blob NOT NULL,
  `expiry_date` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ccv2_no` int(4) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_cc_info`
--

LOCK TABLES `bsi_cc_info` WRITE;
/*!40000 ALTER TABLE `bsi_cc_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `bsi_cc_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_clients`
--

DROP TABLE IF EXISTS `bsi_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_clients` (
  `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) DEFAULT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `title` varchar(16) DEFAULT NULL,
  `street_addr` text,
  `city` varchar(64) DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  `zip` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `fax` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `id_type` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `additional_comments` text,
  `ip` varchar(32) DEFAULT NULL,
  `existing_client` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`client_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_clients`
--

LOCK TABLES `bsi_clients` WRITE;
/*!40000 ALTER TABLE `bsi_clients` DISABLE KEYS */;
INSERT INTO `bsi_clients` VALUES (1,'Nicole','Zuniega','Mr.','222 Dr. Jose Fabella','Mandaluyong','NCR','1550','Philippines','9565977139','','mzunieganicole@gmail.com','fb10039217b62bc57171b185d7946403','','45345346','','::1',1),(2,'Nicole','Zuniega','Ms.','222 Dr. Jose Fabella','Mandaluyong','NCR','1550','Philippines','9565977139','','lofonicole23@gmail.com','a8f5f167f44f4964e6c998dee827110c','Passport','werw45245','werwrt4','::1',1);
/*!40000 ALTER TABLE `bsi_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_configure`
--

DROP TABLE IF EXISTS `bsi_configure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_configure` (
  `conf_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_key` varchar(100) NOT NULL,
  `conf_value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='bsi hotel configurations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_configure`
--

LOCK TABLES `bsi_configure` WRITE;
/*!40000 ALTER TABLE `bsi_configure` DISABLE KEYS */;
INSERT INTO `bsi_configure` VALUES (1,'conf_hotel_name','Labros Adventure Camp and Waterfront'),(2,'conf_hotel_streetaddr','Banus, Concepcion'),(3,'conf_hotel_city','Sablayan'),(4,'conf_hotel_state','Occidental Mindoro'),(5,'conf_hotel_country','Philippines'),(6,'conf_hotel_zipcode','5102'),(7,'conf_hotel_phone','+63917 597 1892'),(8,'conf_hotel_fax',''),(9,'conf_hotel_email','admin@example.com'),(20,'conf_tax_amount','10'),(21,'conf_dateformat','mm/dd/yy'),(22,'conf_booking_exptime','1000'),(25,'conf_enabled_deposit','1'),(26,'conf_hotel_timezone','Asia/Calcutta'),(27,'conf_booking_turn_off','0'),(28,'conf_min_night_booking','1'),(30,'conf_notification_email','admin@example.com'),(31,'conf_price_with_tax','0'),(32,'conf_maximum_global_years','730'),(33,'conf_payment_currency','0'),(34,'conf_invoice_currency','0'),(35,'conf_currency_update_time','1554348345');
/*!40000 ALTER TABLE `bsi_configure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_currency`
--

DROP TABLE IF EXISTS `bsi_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(10) NOT NULL,
  `currency_symbl` varchar(10) NOT NULL,
  `exchange_rate` decimal(20,6) NOT NULL,
  `default_c` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_currency`
--

LOCK TABLES `bsi_currency` WRITE;
/*!40000 ALTER TABLE `bsi_currency` DISABLE KEYS */;
INSERT INTO `bsi_currency` VALUES (6,'PHP','PHP',0.000000,0),(7,'USD','$',1.000000,1),(8,'EUR','EUR',0.000000,0);
/*!40000 ALTER TABLE `bsi_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_email_contents`
--

DROP TABLE IF EXISTS `bsi_email_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_email_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_name` varchar(500) NOT NULL,
  `email_subject` varchar(500) NOT NULL,
  `email_text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_email_contents`
--

LOCK TABLES `bsi_email_contents` WRITE;
/*!40000 ALTER TABLE `bsi_email_contents` DISABLE KEYS */;
INSERT INTO `bsi_email_contents` VALUES (1,'Confirmation Email','Confirmation of your successfull booking in our hotel','<p><strong>Text can be chnage in admin panel</strong></p>\r\n'),(2,'Cancellation Email ','Cancellation Email subject','<p><strong>Text can be chnage in admin panel</strong></p>\r\n');
/*!40000 ALTER TABLE `bsi_email_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_gallery`
--

DROP TABLE IF EXISTS `bsi_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_gallery` (
  `pic_id` int(11) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(11) NOT NULL,
  `capacity_id` int(11) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  PRIMARY KEY (`pic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_gallery`
--

LOCK TABLES `bsi_gallery` WRITE;
/*!40000 ALTER TABLE `bsi_gallery` DISABLE KEYS */;
INSERT INTO `bsi_gallery` VALUES (1,1,4,'1554265587_Mulawin.jpg'),(2,1,6,'1554265597_Duhat Room.jpg'),(3,1,7,'1554265604_Manga 1.jpg'),(4,1,8,'1554265611_Manga 2.jpg'),(5,2,4,'1554265619_Mulawin.jpg');
/*!40000 ALTER TABLE `bsi_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_invoice`
--

DROP TABLE IF EXISTS `bsi_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_invoice` (
  `booking_id` int(10) NOT NULL,
  `client_name` varchar(500) NOT NULL,
  `client_email` varchar(500) NOT NULL,
  `invoice` longtext NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_invoice`
--

LOCK TABLES `bsi_invoice` WRITE;
/*!40000 ALTER TABLE `bsi_invoice` DISABLE KEYS */;
INSERT INTO `bsi_invoice` VALUES (1554368573,'Nicole Zuniega','mzunieganicole@gmail.com','<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1554368573</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Nicole Zuniega</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">04/04/2019</td><td align=\"center\" style=\"background:#ffffff;\">04/05/2019</td><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">1</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Non-Aircon (Kamatsili)</td><td align=\"center\" style=\"background:#ffffff;\">60 Adult  + 4 Child</td><td align=\"right\" style=\"background:#ffffff;\">$6,000.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">$6,000.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"background:#ffffff;\">Tax(10.00%)</td><td align=\"right\" style=\"background:#ffffff;\">(+) $600.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">$6,600.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>'),(1554368890,'Nicole Zuniega','lofonicole23@gmail.com','<table style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tbody><tr><td align=\"left\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\" colspan=\"4\">Booking Details</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Booking Number</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">1554368890</td></tr>\r\n		<tr><td align=\"left\" style=\"background:#ffffff;\">Customer Name</td><td align=\"left\" style=\"background:#ffffff;\" colspan=\"3\">Nicole Zuniega</td></tr>	\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-In Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Check-Out Date</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Nights</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Total Rooms</td></tr>\r\n		<tr><td align=\"center\" style=\"background:#ffffff;\">04/05/2019</td><td align=\"center\" style=\"background:#ffffff;\">04/10/2019</td><td align=\"center\" style=\"background:#ffffff;\">5</td><td align=\"center\" style=\"background:#ffffff;\">2</td></tr>\r\n		<tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\">&nbsp;</td></tr>\r\n		<tr><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Number of Room</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Room Type</td><td align=\"center\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Max Occupancy</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Gross Total</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Aircon (Manga 1)</td><td align=\"center\" style=\"background:#ffffff;\">2 Adult </td><td align=\"right\" style=\"background:#ffffff;\">$6,750.00</td></tr><tr><td align=\"center\" style=\"background:#ffffff;\">1</td><td align=\"center\" style=\"background:#ffffff;\">Aircon (Anapla)</td><td align=\"center\" style=\"background:#ffffff;\">4 Adult  + 2 Child</td><td align=\"right\" style=\"background:#ffffff;\">$78,750.00</td></tr><tr height=\"8px;\"><td align=\"left\" style=\"background:#ffffff;\" colspan=\"4\"></td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Sub Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">$85,500.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"background:#ffffff;\">Tax(10.00%)</td><td align=\"right\" style=\"background:#ffffff;\">(+) $8,550.00</td></tr><tr><td colspan=\"3\" align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Grand Total</td><td align=\"right\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">$94,050.00</td></tr></tbody></table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\">\r\n			\r\n			<tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Any additional requests</td></tr>\r\n			<tr><td align=\"left\" style=\"background:#ffffff;\">efadf</td></tr>\r\n			\r\n			\r\n			\r\n			</table><br /><table  style=\"font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;\" cellpadding=\"4\" cellspacing=\"1\"><tr><td align=\"left\" colspan=\"2\" style=\"font-weight:bold; font-variant:small-caps; background:#eeeeee;\">Payment Details</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps;background:#ffffff;\">Payment Option</td><td align=\"left\" style=\"background:#ffffff;\">Manual</td></tr><tr><td align=\"left\" width=\"30%\" style=\"font-weight:bold; font-variant:small-caps; background:#ffffff;\">Transaction ID</td><td align=\"left\" style=\"background:#ffffff;\">NA</td></tr></table>');
/*!40000 ALTER TABLE `bsi_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_language`
--

DROP TABLE IF EXISTS `bsi_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_title` varchar(255) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `lang_file` varchar(255) NOT NULL,
  `lang_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_language`
--

LOCK TABLES `bsi_language` WRITE;
/*!40000 ALTER TABLE `bsi_language` DISABLE KEYS */;
INSERT INTO `bsi_language` VALUES (1,'English','en','english.php',1),(2,'French','fr','french.php',0),(3,'German','de','german.php',0),(4,'Greek','el','greek.php',0),(5,'Spanish','es','espanol.php',0),(6,'Italian','it','italian.php',0),(7,'Dutch','nl','dutch.php',0),(8,'Polish','pl','polish.php',0),(9,'Portuguese','pt','portuguese.php',0),(10,'Russian','ru','russian.php',0),(11,'Turkish','tr','turkish.php',0),(12,'Thai','th','thai.php',0),(13,'Chinese','zh-CN','chinese.php',0),(14,'Indonesian','id','indonesian.php',0),(15,'Romanian','ro','romanian.php',0),(17,'Japanese','ja','japanese.php',0);
/*!40000 ALTER TABLE `bsi_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_payment_gateway`
--

DROP TABLE IF EXISTS `bsi_payment_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_payment_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(255) NOT NULL,
  `gateway_code` varchar(50) NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_payment_gateway`
--

LOCK TABLES `bsi_payment_gateway` WRITE;
/*!40000 ALTER TABLE `bsi_payment_gateway` DISABLE KEYS */;
INSERT INTO `bsi_payment_gateway` VALUES (1,'PayPal','pp','phpdev_1330251667_biz@aol.com',1),(2,'Manual','poa','null',1),(3,'Credit Card (offline)','cc','null',1),(4,'Authorize.Net','an','8nZ8py3MY=|=6936uG8m8hKFRP6A=|=bestsoftinc',1),(5,'2Checkout','2co','2002812',1),(6,'Stripe','st','sk_test_HTGxkUdVdAGd3t0dFqFomE40#pk_test_c6KEy6DebkozRP0V9ysSDwVq#1',1);
/*!40000 ALTER TABLE `bsi_payment_gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_priceplan`
--

DROP TABLE IF EXISTS `bsi_priceplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_priceplan` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(10) DEFAULT NULL,
  `capacity_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sun` decimal(10,2) DEFAULT '0.00',
  `mon` decimal(10,2) DEFAULT '0.00',
  `tue` decimal(10,2) DEFAULT '0.00',
  `wed` decimal(10,2) DEFAULT '0.00',
  `thu` decimal(10,2) DEFAULT '0.00',
  `fri` decimal(10,2) DEFAULT '0.00',
  `sat` decimal(10,2) DEFAULT '0.00',
  `default_plan` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`plan_id`),
  KEY `priceplan` (`roomtype_id`,`capacity_id`,`start_date`,`end_date`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_priceplan`
--

LOCK TABLES `bsi_priceplan` WRITE;
/*!40000 ALTER TABLE `bsi_priceplan` DISABLE KEYS */;
INSERT INTO `bsi_priceplan` VALUES (7,1,4,'0000-00-00','0000-00-00',2500.00,2500.00,2500.00,2500.00,2500.00,2500.00,2500.00,1),(8,2,4,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(11,1,6,'0000-00-00','0000-00-00',5000.00,5000.00,5000.00,5000.00,5000.00,5000.00,5000.00,1),(12,2,6,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(13,1,7,'0000-00-00','0000-00-00',1500.00,1500.00,1500.00,1500.00,1500.00,1500.00,1500.00,1),(14,2,7,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(15,1,8,'0000-00-00','0000-00-00',2000.00,2000.00,2000.00,2000.00,2000.00,2000.00,2000.00,1),(16,2,8,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(17,1,9,'0000-00-00','0000-00-00',2000.00,2000.00,2000.00,2000.00,2000.00,2000.00,2000.00,1),(18,2,9,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(19,1,10,'0000-00-00','0000-00-00',17500.00,17500.00,17500.00,17500.00,17500.00,17500.00,17500.00,1),(20,2,10,'0000-00-00','0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,1),(21,1,11,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(22,2,11,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(23,1,12,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(24,2,12,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(25,1,13,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(26,2,13,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(27,1,14,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(28,2,14,'0000-00-00','0000-00-00',800.00,800.00,800.00,800.00,800.00,800.00,800.00,1),(29,1,15,'0000-00-00','0000-00-00',6000.00,6000.00,6000.00,6000.00,6000.00,6000.00,6000.00,1),(30,2,15,'0000-00-00','0000-00-00',6000.00,6000.00,6000.00,6000.00,6000.00,6000.00,6000.00,1);
/*!40000 ALTER TABLE `bsi_priceplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_reservation`
--

DROP TABLE IF EXISTS `bsi_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookings_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_reservation`
--

LOCK TABLES `bsi_reservation` WRITE;
/*!40000 ALTER TABLE `bsi_reservation` DISABLE KEYS */;
INSERT INTO `bsi_reservation` VALUES (3,1554368573,38,2),(4,1554368890,31,1),(5,1554368890,33,1);
/*!40000 ALTER TABLE `bsi_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_room`
--

DROP TABLE IF EXISTS `bsi_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_room` (
  `room_ID` int(10) NOT NULL AUTO_INCREMENT,
  `roomtype_id` int(10) DEFAULT NULL,
  `room_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `capacity_id` int(10) DEFAULT NULL,
  `no_of_child` int(11) NOT NULL DEFAULT '0',
  `extra_bed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`room_ID`),
  KEY `roomtype_id` (`roomtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_room`
--

LOCK TABLES `bsi_room` WRITE;
/*!40000 ALTER TABLE `bsi_room` DISABLE KEYS */;
INSERT INTO `bsi_room` VALUES (25,1,'25',4,1,0),(26,1,'26',4,1,0),(27,1,'27',6,2,0),(28,1,'28',6,2,0),(29,1,'29',6,2,0),(30,1,'30',6,2,0),(31,1,'31',7,1,0),(32,1,'32',8,2,0),(33,1,'33',10,2,0),(34,2,'34',11,1,0),(35,2,'35',12,1,0),(36,2,'36',13,1,0),(37,2,'37',14,1,0),(38,2,'38',15,10,0);
/*!40000 ALTER TABLE `bsi_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_roomtype`
--

DROP TABLE IF EXISTS `bsi_roomtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_roomtype` (
  `roomtype_ID` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(500) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`roomtype_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_roomtype`
--

LOCK TABLES `bsi_roomtype` WRITE;
/*!40000 ALTER TABLE `bsi_roomtype` DISABLE KEYS */;
INSERT INTO `bsi_roomtype` VALUES (1,'Aircon',''),(2,'Non-Aircon','');
/*!40000 ALTER TABLE `bsi_roomtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsi_special_offer`
--

DROP TABLE IF EXISTS `bsi_special_offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsi_special_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `room_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `price_deduc` decimal(10,2) NOT NULL,
  `min_stay` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsi_special_offer`
--

LOCK TABLES `bsi_special_offer` WRITE;
/*!40000 ALTER TABLE `bsi_special_offer` DISABLE KEYS */;
INSERT INTO `bsi_special_offer` VALUES (1,'New Graduates Offer','2019-04-03','2019-04-30','0',10.00,2),(2,'Summer Offer','2019-05-01','2019-05-31','0',5.00,2),(3,'Back to School Offer','2019-06-01','2019-06-30','0',5.00,0);
/*!40000 ALTER TABLE `bsi_special_offer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-04 17:44:45
